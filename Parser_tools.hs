{- 
	Author: Staffan Arvidsson
	Module that reads Fasta files and prints contigs to a file
-}	

module Parser_tools
( getStrings
, readFasta
, outputContigs
) where

import qualified Data.ByteString.Lazy.Char8 as C

{- 
-------------------------------------------------------------------
	Reads a file in Fasta format and returns a list of pairs with 
	(name, sequence). Both of type ByteString 
-------------------------------------------------------------------
-}
readFasta file_path = do 
	input <- getStrings file_path
	let fasta = parse input C.empty C.empty []
	return fasta

-- tail recursive function!
parse :: [C.ByteString] -> C.ByteString -> C.ByteString -> [(C.ByteString, C.ByteString)] -> [(C.ByteString, C.ByteString)]
parse [] seq name result = (name,seq):result
parse (x:xs) seq name res
 | (C.isPrefixOf (C.pack ">") x) && (not $ C.null name) = parse xs C.empty (C.tail x) ((name,seq):res) 
 | (C.isPrefixOf (C.pack ">") x)						= parse xs C.empty (C.tail x) res
 | otherwise 											= parse xs (C.append seq x) name res


-----------------------------------------------------------------------------------------
--- OUTPUT contigs
-----------------------------------------------------------------------------------------


outputContigs :: FilePath -> [C.ByteString] -> IO()
outputContigs filePath contigList = do
	C.writeFile filePath $ C.pack "" -- remove a possible old file with the same name
	outputContigs_h filePath contigList 1

outputContigs_h :: FilePath -> [C.ByteString] -> Int -> IO()
outputContigs_h _ [] _ = return ()
outputContigs_h filePath (contig:remainingContigs) num = do
	C.appendFile filePath $ C.pack ">Contig: "
	C.appendFile filePath $ C.pack (show num ++ " \n")
	C.appendFile filePath contig
	C.appendFile filePath $ C.pack "\n"
	outputContigs_h filePath remainingContigs $ num+1
	









{- 
-------------------------------------------------------------------
	Takes a list of dna strings separated by a line-break and returns a list of 
	ByteStrings with one sequence in each position. Removes all empty lines
-------------------------------------------------------------------
-} 
getStrings :: FilePath -> IO [C.ByteString]
getStrings file_path = do
	content <- C.readFile file_path
	let resultList = removeEmpty $ C.split '\n' content
	return resultList

removeEmpty :: [C.ByteString] -> [C.ByteString]
removeEmpty list = filter (\x -> not (C.empty ==x)) list








{-
-- non tail-recursive function - slow!

readFasta file_path = do 
	input <- getStrings file_path
	let fasta = parseFasta input C.empty C.empty
	return fasta

parseFasta :: [C.ByteString] -> C.ByteString -> C.ByteString -> [(C.ByteString, C.ByteString)]
parseFasta [] seq name = (name,seq):[]
parseFasta (x:xs) seq name
 | (C.isPrefixOf (C.pack ">") x) && (not $ C.null name) 	= (name, seq):(parseFasta xs C.empty (C.tail x))
 | (C.isPrefixOf (C.pack ">") x)						= parseFasta xs C.empty (C.tail x)
 | otherwise 											= parseFasta xs (C.append seq x) name
-}
	
