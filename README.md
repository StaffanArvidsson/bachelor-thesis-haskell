# README #
* The main code is located in the ParDeBruijn.hs file (main function, creation of de Bruijjn Graph and traversal).
* Parser_tools.hs contains a function for parsing FASTA files and also a function that prints generated Contigs to a specified outfile. 
* The folder 'input' contains a set of FASTA files that can be used for testing. 

Compile by: ghc -O2 -threaded -main-is ParDeBruijn --make ParDeBruijn -rtsopts -eventlog

Run by: ./ParDeBruijn <pathToInput> <k value> +RTS -N