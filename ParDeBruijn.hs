{-
	Author: Staffan Arvidsson
	Heuristic version of de Bruijn graphs
	Uses HashMap.Strict for storing k-mers
	Stores Key (PrevChar, NextChar) 
-}

module ParDeBruijn
( 
	main
) where



-- compile: 		ghc -O2 -threaded -main-is ParDeBruijn --make ParDeBruijn -rtsopts -eventlog
-- run (get info):	time ./ParDeBruijn ../input/ERR_50k_reads.fasta 25 +RTS -h -N -lf  -sstderr
-- run: 			time ./ParDeBruijn ../input/ERR_50k_reads.fasta 25 +RTS -N
-----------------------------------------------------------------------------------------
-- 		Import libraries
-----------------------------------------------------------------------------------------
import qualified Parser_tools as P
import qualified Data.ByteString.Lazy.Char8 as C
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HM
import qualified Control.Parallel.Strategies as PS
import Data.Set (Set)
import qualified Data.Set as Set
import Control.DeepSeq
import System.IO
import System.Environment
import Data.Int
import Data.Maybe


-----------------------------------------------------------------------------------------
-- 		Type synonyms
-----------------------------------------------------------------------------------------
type Sequence = C.ByteString
type SeqID = C.ByteString
type FastaRec = (SeqID, Sequence)
type FastaList = [FastaRec]
type HashIndex = (Sequence, (Char,Char))
type DeBruijnGraph = HashMap Sequence (Char, Char)

-----------------------------------------------------------------------------------------
-- 		Parameters for parallel tuning 
-----------------------------------------------------------------------------------------
chunkSize = 100

outFile = "../output/Contigs_parallel.txt"


-----------------------------------------------------------------------------------------
-- 		CODE
-----------------------------------------------------------------------------------------

--main :: IO (Int)
main = do  
	[file_path, k_str] <- getArgs
	seqList <- P.readFasta file_path
	let { k = (read k_str :: Int) ;
		hashIndexes = concat $ (map (createHashIndexes k) seqList `PS.using` PS.parListChunk chunkSize PS.rdeepseq) ;
		cleaned = genUUMap hashIndexes ;
		contigs = getContigs k cleaned ;
	}
	putStrLn $ show $ length contigs
	--P.outputContigs outFile contigs
	return (contigs)

createHashIndexes :: Int -> FastaRec -> [HashIndex]
createHashIndexes k (_, seq) = createHashList (C.tail seq) k (C.head seq) []


createHashList :: Sequence -> Int -> Char -> [HashIndex] -> [HashIndex]
createHashList seq k prev resList
 | fromIntegral( C.length seq ) < k+1 	= resList
 | otherwise	= createHashList (C.tail seq) k (C.head seq)   (((C.take (fromIntegral k :: Int64) seq), (prev, (C.head $ C.drop (fromIntegral k :: Int64) seq))):resList)

-- folds over the list of hashindexes using reduceToUU
genUUMap :: [HashIndex] -> DeBruijnGraph
genUUMap hashIndexes = 
	snd $ foldl (reduceToUU) (Set.empty, HM.empty) hashIndexes

-- Checks if the k-mer has been found before (part of the Set)
-- if found before - need to check if the prev and next bases matches
-- else insert the k-mere in the "found Set" and insert into the HashMap
reduceToUU :: (Set Sequence, DeBruijnGraph) -> HashIndex -> (Set Sequence, DeBruijnGraph)
reduceToUU (set, graph) index 
	| found_before		= check_further set graph index 
	| otherwise 		= (Set.insert (fst index) set, HM.insert (fst index) (snd index) graph)
	where found_before = Set.member (fst index) set

-- Checks if the current k-mer has identical prev and next bases as before
-- if identical - do nothing
-- if not found in HashMap - found and removed before - return current set and graph
-- else remove the k-mer that now has proven to not be unique
check_further :: Set Sequence -> DeBruijnGraph -> HashIndex -> (Set Sequence, DeBruijnGraph)
check_further set graph index
	| lookup == snd index	= (set, graph)
	| lookup == ('_','_')	= (set, graph)
	| otherwise				= (set, HM.delete (fst index) graph) 	
	where lookup = HM.lookupDefault ('_','_') (fst index) graph





-----------------------------------------------------------------------------------------
-- 		Traverse the map!
-----------------------------------------------------------------------------------------

getContigs :: Int -> DeBruijnGraph -> [Sequence]
getContigs k graph = returnAllContigs (fromIntegral k ::Int64) graph []

returnAllContigs :: Int64 -> DeBruijnGraph -> [Sequence]-> [Sequence]
returnAllContigs k graph contigList
	| HM.null graph	= contigList
	| otherwise		= 
		returnAllContigs k newGraph (newContig:contigList)
		where
			[nextKmer] = take 1 $ HM.keys graph
			(prev,next) = HM.lookupDefault ('_','_') nextKmer graph
			graph2 = HM.delete nextKmer graph
			(newContig, newGraph) = travContig k nextKmer prev next graph2 
		

travContig :: Int64 -> Sequence -> Char -> Char -> DeBruijnGraph -> (Sequence, DeBruijnGraph)
travContig k seq prevChar nextChar graph = (C.append left right, graph3)
	where
		(left,graph2) = travContig_left k  (C.cons prevChar seq) graph
		(right_pre,graph3) = travContig_right k (C.snoc seq nextChar) graph2
		right = C.drop k right_pre

-------------------------------------------------------------------------------------
-- traverse left

travContig_left :: Int64 -> Sequence -> DeBruijnGraph -> (Sequence, DeBruijnGraph)
travContig_left k contig graph =
	if (isJust valFromGraph)
		then travContig_left k newContig newGraph
		else (contig, graph)
	where
		valFromGraph = getValueFromGraph (C.take k contig) graph
		(prevChar,_,newGraph) = fromJust valFromGraph
		newContig = C.cons prevChar contig

-------------------------------------------------------------------------------------		
-- traverse right

travContig_right :: Int64 -> Sequence -> DeBruijnGraph -> (Sequence, DeBruijnGraph)
travContig_right k contig graph =
	if (isJust valFromGraph)
		then travContig_right k newContig newGraph
		else (contig, graph)
	where
		lastK = C.drop ((C.length contig)-k) contig
		valFromGraph = getValueFromGraph lastK graph
		(_,nextChar,newGraph) = fromJust valFromGraph
		newContig = C.snoc contig nextChar


getValueFromGraph :: Sequence -> DeBruijnGraph -> Maybe (Char,Char,DeBruijnGraph)
getValueFromGraph k_mer graph =
	if (isJust maybeValue)
		then Just (prevChar, nextChar, newGraph)
		else Nothing
	where
		maybeValue = HM.lookup k_mer graph
		(prevChar, nextChar) = fromJust maybeValue
		newGraph = HM.delete k_mer graph
	











-----------------------------------------------------------------------------------------
-- 		Code for generating performance graph
-----------------------------------------------------------------------------------------




{-
-- For generating the performance graph

import Text.Printf
import Control.Exception
import System.CPUTime

printDiff end start =
	printf "time: %0.3f sec\n" (diff :: Double)
	where
		diff = (fromIntegral (end-start))/(10^12)

main = do
	[file_path, k_str] <- getArgs
	bef_readFasta <- getCPUTime
	seqList <- P.readFasta file_path
	putStrLn ("Number of records: " ++ (show $ length seqList))
	let k = (read k_str :: Int)
	bef_createHashInd <- getCPUTime
	let list_of_lists = map (createHashIndexes k) seqList `PS.using` PS.parListChunk chunkSize PS.rdeepseq
	putStrLn ("Number of lists in the list_of_list: " ++ ( show $ length list_of_lists))
	bef_concat <- getCPUTime
	let hash_ind = concat list_of_lists
	--let hash_ind = my_concat $ (map (createHashIndexes k) seqList `PS.using` PS.parListChunk chunkSize PS.rdeepseq)
	
	
	putStrLn ("Length of hash_indexes: "++ (show $ length hash_ind))
	bef_genUUMap <- getCPUTime
	let cleaned = genUUMap hash_ind
	putStrLn ("Size of the final HashMap: " ++ ( show $ HM.size cleaned ))
	bef_getContigs <- getCPUTime
	let	contigs = getContigs k cleaned
	putStrLn ("Number of contigs generated: " ++ ( show $ length contigs ))
	finished <-getCPUTime
	
	putStr "Parse Fasta, "
	printDiff bef_createHashInd bef_readFasta
	putStr "create list_of_lists, "
	printDiff bef_concat bef_createHashInd
	
	putStr "Create final Hashindexes, "
	printDiff bef_genUUMap bef_concat
	putStr "Gen UU map, "
	printDiff bef_getContigs bef_genUUMap 
	putStr "Get Contigs, "
	printDiff finished bef_getContigs 
	
	putStrLn $ show $ length contigs
	return (contigs)

-}
